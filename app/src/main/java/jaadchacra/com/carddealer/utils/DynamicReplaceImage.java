package jaadchacra.com.carddealer.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * Used to fetch image from Resources and replace
 * source of ImageView dynamically
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public class DynamicReplaceImage {
    /**
     * Fetches image from Resources and replaces
     * source of ImageView at run-time
     */
    public static void replaceImage(String imageName, ImageView imageView, Context context) {
        String uri = "@drawable/" + imageName;  // myresource (without the extension) is the image
        int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
        Drawable res = context.getResources().getDrawable(imageResource);
        imageView.setImageDrawable(res);
    }
}
