package jaadchacra.com.carddealer.utils;

import android.content.Context;

import jaadchacra.com.carddealer.deckofcards.lib.DeckOfCards;

/**
 * class for objects which require one instance
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public class MySingleton extends DeckOfCards {
    private static MySingleton mInstance;
    private static Context mCtx;
    private DeckOfCards deckOfCards;


    /**
     * A private Constructor prevents any other
     * class from instantiating.
     */
    private MySingleton(Context context) {
        mCtx = context;
        deckOfCards = new DeckOfCards();
    }

    /**
     * Static 'instance' method
     */
    public static MySingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySingleton(context);
        }
        return mInstance;
    }

    /**
     * returns the instance of deckOfCards
     * Creates a new one if it didn't exist
     *
     * @return deckOfCards
     */
    public DeckOfCards getDeckOfCards() {
        if (deckOfCards == null) {
            deckOfCards = new DeckOfCards();
        }
        return deckOfCards;
    }

    /**
     * returns new deckOfCards
     *
     * @return deckOfCards
     */
    public DeckOfCards replaceDeckOfCards() {
        deckOfCards = new DeckOfCards();
        return deckOfCards;
    }
}


