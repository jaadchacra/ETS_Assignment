package jaadchacra.com.carddealer.deckofcards.lib;

import java.util.ArrayList;

/**
 * Represents a deck of 52 cards and two main operations that are made on them:
 * Shuffling and Dealing.
 *
 * @author Jaad Chacra
 * @version 1.0
 */

public class DeckOfCards {

    /**
     * An array which can contain up to 52 cards.
     */
    private Card[] deckOfCards;

    /**
     * Keeps track of the number of cards that have been dealt from
     * the deck so far.
     */
    private int cardsUsed;

    /**
     * Constructor. The deck contains 52 Cards.
     * The Cards are in sorted order initially.
     */
    public DeckOfCards() {
        deckOfCards = new Card[52];

        int cardCount = 0;

        for (Suits s : Suits.values()) {
            for (Ranks r : Ranks.values()) {
                deckOfCards[cardCount++] = new Card(s, r);
            }
        }
    }

    /**
     * Shuffles the remaining cards in the deck
     */
    public void shuffle() {
        for (int i = deckOfCards.length - 1 - cardsUsed; i > 0; i--) {
            int rand = (int) (Math.random() * (i + 1));
            Card temp = deckOfCards[i];
            deckOfCards[i] = deckOfCards[rand];
            deckOfCards[rand] = temp;
        }
    }

    /**
     * Pops the next card off the deck and return it.
     *
     * @return the card which was at the top of the deck.
     * @throws IllegalStateException if deck was empty before calling the method
     */
    public Card dealCard() {
        if (cardsUsed == deckOfCards.length)
            throw new IllegalStateException("Deck is empty.");
        cardsUsed++;
        return deckOfCards[cardsUsed - 1];
    }

    /**
     * Returns a String representation of the remaining Cards in
     * the deck
     *
     * @return value contains only letters
     */
    public String toString() {
        String deckOfCardsName = "";
        for (int i = cardsUsed; i < deckOfCards.length; i++) {
            deckOfCardsName += deckOfCards[i];
        }
        return deckOfCardsName;
    }

    /**
     * Returns an ArrayList representation of the remaining Cards in
     * the deck
     * @return ArrayList<Card>
     */
    public ArrayList<Card> toArrayList() {
        ArrayList<Card> cardArrayList = new ArrayList<>();
        for (int i = cardsUsed; i < deckOfCards.length; i++) {
            cardArrayList.add(deckOfCards[i]);
        }
        return cardArrayList;
    }

    /**
     * Returns the number of cards remaining in the deck
     *
     * @return number of cards remaining
     */
    public int size() {
        return 52 - cardsUsed;
    }
}
