package jaadchacra.com.carddealer.deckofcards.lib;

/**
 * Suits used for the Card Object
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public enum Suits {
    HEARTS,
    SPADES,
    CLUBS,
    DIAMONDS;
}