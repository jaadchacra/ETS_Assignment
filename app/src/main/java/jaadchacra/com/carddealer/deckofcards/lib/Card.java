package jaadchacra.com.carddealer.deckofcards.lib;

/**
 * Represents Card object which is used in deck of cards
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public class Card {

    private Suits suit;
    private Ranks rank;

    /**
     * Constructor with a specified suit and rank
     *
     * @param suit the suit of the card
     * @param rank the rank of the card
     */
    public Card(Suits suit, Ranks rank) {
        this.suit = suit;
        this.rank = rank;
    }

    /**
     * A String representation of the card consisting of its suit and rank
     * Lowercase is used to fetch image representing the card from resources
     * at runtime
     *
     * @return lowercase string represenation of card
     */
    public String toString() {
        String uppercaseCard = String.format(rank + "_" + suit);
        return uppercaseCard.toLowerCase();
    }

}
