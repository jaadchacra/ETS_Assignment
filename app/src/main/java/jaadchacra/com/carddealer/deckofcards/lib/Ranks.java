package jaadchacra.com.carddealer.deckofcards.lib;

/**
 * Ranks used for the Card Object
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public enum Ranks {
    ACE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING;
}