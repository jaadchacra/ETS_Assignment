package jaadchacra.com.carddealer.dealer;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import jaadchacra.com.carddealer.utils.DynamicReplaceImage;
import jaadchacra.com.carddealer.utils.MySingleton;
import jaadchacra.com.carddealer.R;
import jaadchacra.com.carddealer.dealer.peekingdeck.PeekingDeckActivity;
import jaadchacra.com.carddealer.deckofcards.lib.Card;

/**
 * This class contains the interface to test
 * deckofcards.lib which contains the functions needed
 * for a dealer to interact with a deck of cards
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public class DealerActivity extends AppCompatActivity {

    public ImageView deckOfCardsImageView, cardImageView;
    public Button shuffleButton, replaceButton, peekButton;
    public Card dealtCard;
    public String deckImageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer);

        // Initializes the Views
        init();

        // Sets the Click Listeners
        setClickListeners();
    }

    /**
     * Initializes the Views
     */
    public void init() {
        deckOfCardsImageView = (ImageView) findViewById(R.id.deckOfCardsImageView);
        cardImageView = (ImageView) findViewById(R.id.cardImageView);
        shuffleButton = (Button) findViewById(R.id.shuffleButton);
        replaceButton = (Button) findViewById(R.id.replaceButton);
        peekButton = (Button) findViewById(R.id.peekButton);
    }

    /**
     * Sets all the Click Listeners
     */
    public void setClickListeners(){

        // Shuffles the available cards in the deck
        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getInstance(getApplicationContext()).getDeckOfCards().shuffle();
            }
        });

        // Replaces the deck of cards with a new deck
        replaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getInstance(getApplicationContext()).replaceDeckOfCards();

                // Incase the deck was made invisible in isReducable() method
                deckOfCardsImageView.setVisibility(View.VISIBLE);

                deckImageName = "deck_" + MySingleton.getInstance(getApplicationContext()).getDeckOfCards().size();
                DynamicReplaceImage.replaceImage(deckImageName, deckOfCardsImageView, getBaseContext());
            }
        });

        // Creates an Intent to PeekingDeckActivity
        peekButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), PeekingDeckActivity.class);
                startActivity(intent);
            }
        });

        // Deals a card and displays it
        deckOfCardsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isReducable()) {
                    // The images are named after number of cards left, prefixed with "deck_"
                    deckImageName = "deck_" + MySingleton.getInstance(getApplicationContext()).getDeckOfCards().size();
                    // Gets image from Resources using provided image name, replaces src of ImageView with the image
                    DynamicReplaceImage.replaceImage(deckImageName, deckOfCardsImageView, getBaseContext());
                }

                try {
                    dealtCard = MySingleton.getInstance(getApplicationContext()).getDeckOfCards().dealCard();
                } catch (IllegalStateException e) {
                    Toast toast = Toast.makeText(getBaseContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT);
                    toast.show();
                }
                // Gets image from Resources using provided image name, replaces src of ImageView with the image
                DynamicReplaceImage.replaceImage(dealtCard.toString(), cardImageView, getBaseContext());
            }
        });
    }

    /**
     * In order to reduce the images that give the effect of reducing
     * deck of cards, 15 images were selected to be displayed when a
     * certain amount of cards are left.
     *
     * @return true if number of cards in deck equals one of the 15
     *          selected numbers used to represent images
     */
    public boolean isReducable() {
        switch (MySingleton.getInstance(getApplicationContext()).getDeckOfCards().size()) {
            case 51:
                return true;
            case 45:
                return true;
            case 40:
                return true;
            case 35:
                return true;
            case 30:
                return true;
            case 25:
                return true;
            case 20:
                return true;
            case 15:
                return true;
            case 10:
                return true;
            case 6:
                return true;
            case 5:
                return true;
            case 4:
                return true;
            case 3:
                return true;
            case 2:
                return true;
            case 1:
                //Makes the deckOfCards Gone
                deckOfCardsImageView.setVisibility(View.GONE);
        }
        return false;
    }
}
