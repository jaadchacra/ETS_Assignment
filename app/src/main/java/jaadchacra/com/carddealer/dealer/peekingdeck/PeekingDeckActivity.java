package jaadchacra.com.carddealer.dealer.peekingdeck;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import jaadchacra.com.carddealer.utils.MySingleton;
import jaadchacra.com.carddealer.R;

/**
 * Displays all the cards in the deck that haven't been dealt
 * in a RecyclerView
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public class PeekingDeckActivity extends AppCompatActivity {

    private GridLayoutManager lLayout;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter cardAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peeking_deck);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Makes the ViewHolders dispayed in columns of 3
        lLayout = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(lLayout);

        // gettimg the Deck of Cards from MySingleton to set the CardAdapter for RecyclerView
        cardAdapter = new CardAdapter(MySingleton.getInstance(getApplicationContext()).getDeckOfCards().toArrayList(), this);
        recyclerView.setAdapter(cardAdapter);
    }
}
