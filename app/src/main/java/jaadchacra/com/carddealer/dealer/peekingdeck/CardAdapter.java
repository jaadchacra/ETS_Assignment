package jaadchacra.com.carddealer.dealer.peekingdeck;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jaadchacra.com.carddealer.R;
import jaadchacra.com.carddealer.deckofcards.lib.Card;
import jaadchacra.com.carddealer.utils.DynamicReplaceImage;

/**
 * An adapter which holds all the cards available
 * in the deck
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public class CardAdapter extends RecyclerView.Adapter<CardViewHolder> {
    private Context context;
    // contains the cards that haven't been dealt from the deck
    ArrayList<Card> deckOfCards;

    /**
     * Class constructor.
     */
    public CardAdapter(ArrayList<Card> deckOfCards, Context context) {
        super();
        this.deckOfCards = deckOfCards;
        this.context = context;
    }

    /**
     * Called when RecyclerView needs a new CardViewHolder
     *
     * @param parent   ViewGroup where View will be added
     * @param viewType view type of the new View
     */
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_card, parent, false);

        CardViewHolder viewHolder = new CardViewHolder(v);

        return viewHolder;
    }

    /**
     * Binds ViewHolder to Adapter at given position
     *
     * @param holder   viewholder to bind
     * @param position in adapter to bind
     */
    @Override
    public void onBindViewHolder(final CardViewHolder holder, int position) {
        final Card card = deckOfCards.get(position);
        DynamicReplaceImage.replaceImage(card.toString(), holder.cardImageView, context);
    }

    /**
     * returns the number of cards in the deck
     *
     * @return int number of cards in deck
     */
    @Override
    public int getItemCount() {
        return deckOfCards.size();
    }
}
