package jaadchacra.com.carddealer.dealer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Places ETS logo while app loads for smooth transition
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, DealerActivity.class);
        startActivity(intent);
        finish();
    }
}
