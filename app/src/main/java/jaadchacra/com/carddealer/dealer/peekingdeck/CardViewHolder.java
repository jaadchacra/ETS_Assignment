package jaadchacra.com.carddealer.dealer.peekingdeck;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import jaadchacra.com.carddealer.R;

/**
 * ViewHolder class which holds an ImageView
 * which represents a Card
 *
 * @author Jaad Chacra
 * @version 1.0
 */
public class CardViewHolder extends RecyclerView.ViewHolder {
    /**
     * ImageView representing a Card
     * */
    public ImageView cardImageView;

    /**
     * Constructor initializing the ImageView
     * */
    public CardViewHolder(View itemView) {
        super(itemView);
        cardImageView = (ImageView) itemView.findViewById(R.id.cardImageView);
    }
}
