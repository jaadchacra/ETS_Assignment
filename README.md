# ETS_Assignment
Android solution to Deck of Cards Pre-Interview Assignment

Kindly find the apk attached as well

The main requirement can be found in deckofcards.lib package

Everything else is for the interface and to test and use the code


**Screenshots**:

Initial State

![Screenshot](No cards Dealt.jpg)

One Card Dealt

![Screenshot](One card Deakt.jpg)

Many Cards Dealt

![Screenshot](Many Cards Dealt.jpg)

Unshuffled Cards

![Screenshot](Unshuffled Cards.jpg)

Shuffled Cards

![Screenshot](Shuffled Cards.jpg)
